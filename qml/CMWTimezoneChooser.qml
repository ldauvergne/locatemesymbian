import QtQuick 1.1
import com.nokia.symbian 1.1
import com.nokia.extras 1.1

import "javascript/SaveandLoad.js" as SaveandLoad
import "javascript/Storage.js" as Storage
import "javascript/LocationRequest.js" as LocationRequest
/*
import "javascript/UIConstants.js" as UIConstants
import "javascript/APSettings.js" as APSettings


*/

Item {

    Background{}

    CMWHeader{
        id:top
        title: qsTr("Choose Timezone")
        anchors.top:parent.top
        width:parent.width
        height: 63
    }

    Rectangle {

        height : 200
        width: 360
        color:"black"
        opacity: 0.6
        radius:10
        anchors{

            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
        }

        Tumbler {


            anchors {
                fill: parent
            }

            columns: timezoneColumn

        }





        function initializeDataModels() {

            for (var tz = -11; tz <= 12; tz++) {

                tzList.append({"value" : tz});

            }

        }

        Component.onCompleted: {

            initializeDataModels();

        }

        TumblerColumn {

            id: timezoneColumn

            items: ListModel { id: tzList }



            selectedIndex: datePerso.timezone+11

        }

    }
    ToolBar {
        id:toolbar

        anchors{
            bottom: parent.bottom
        }

        tools: ToolBarLayout {
            id: toolBarLayout

            ButtonRow{

                anchors.fill: parent
                ToolButton {
                    text: "Valid"
                    onClicked: {
                        console.log("Valid")

                        datePerso.timezone=timezoneColumn.selectedIndex-11
                        Storage.setSetting("MY_TIMEZONE", timezoneColumn.selectedIndex-11)

                        mysunrise= LocationRequest.gettime(mylongitude,mylatitude,1)
                        mysunset= LocationRequest.gettime(mylongitude,mylatitude,0)

                        mainpage.state="SETTINGSPAGE"

                    }
                }/*
                ToolButton {
                    text: "Cancel"
                    onClicked: {
                        console.log("Cancel")
                        mainpage.state="SETTINGSPAGE"
                    }
                }*/

            }
        }
    }

}
