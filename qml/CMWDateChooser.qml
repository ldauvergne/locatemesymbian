import QtQuick 1.1
import com.nokia.symbian 1.1
import com.nokia.extras 1.1

import "javascript/SaveandLoad.js" as SaveandLoad
import "javascript/Storage.js" as Storage
import "javascript/LocationRequest.js" as LocationRequest
/*
import "javascript/UIConstants.js" as UIConstants
import "javascript/APSettings.js" as APSettings


*/

Item {

    Background{}

    CMWHeader{
        id:top
        title: qsTr("Choose Date")
        anchors.top:parent.top
        width:parent.width
        height: 63
    }

    Rectangle {

        height : 200
        width: 360
        color:"black"
        opacity: 0.6
        radius:10
        anchors{

            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
        }

        Tumbler {


            anchors {
                fill: parent
            }

            columns: [ dayColumn, monthColumn ]

        }





        function initializeDataModels() {

            for (var day = 1; day <= 31; day++) {

                dayList.append({"value" : day});

            }

        }

        Component.onCompleted: {

            initializeDataModels();

        }

        TumblerColumn {

            id: dayColumn

            items: ListModel { id: dayList }



            selectedIndex: datePerso.dateperso.getDate()-1

        }

        TumblerColumn {

            id: monthColumn

            items: ListModel {

                ListElement { value: "Jan" }

                ListElement { value: "Feb" }

                ListElement { value: "Mar" }

                ListElement { value: "Apr" }

                ListElement { value: "May" }

                ListElement { value: "Jun" }

                ListElement { value: "Jul" }

                ListElement { value: "Aug" }

                ListElement { value: "Sep" }

                ListElement { value: "Oct" }

                ListElement { value: "Nov" }

                ListElement { value: "Dec" }

            }
            selectedIndex: datePerso.dateperso.getMonth()

        }

    }
    ToolBar {
        id:toolbar

        anchors{
            bottom: parent.bottom
        }

        tools: ToolBarLayout {
            id: toolBarLayout

            ButtonRow{

                anchors.fill: parent
                ToolButton {
                    text: "Valid"
                    onClicked: {
                        console.log("Valid")

                        datePerso.dateperso=new Date(datePerso.dateperso.getFullYear(),monthColumn.selectedIndex,dayColumn.selectedIndex+1)
                        SaveandLoad.savedate()

                        console.log( LocationRequest.gettime(mylongitude,mylatitude,1))
                        console.log( LocationRequest.gettime(mylongitude,mylatitude,0))

                        mysunrise= LocationRequest.gettime(mylongitude,mylatitude,1)
                        mysunset= LocationRequest.gettime(mylongitude,mylatitude,0)

                        mainpage.state="SETTINGSPAGE"

                    }
                }/*
                ToolButton {
                    text: "Cancel"
                    onClicked: {
                        console.log("Cancel")

                        mainpage.state="SETTINGSPAGE"
                    }
                }*/

            }
        }
    }

}
