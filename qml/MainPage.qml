// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1
import QtMobility.location 1.1


import "javascript/UIConstants.js" as UIConstants
import "javascript/APSettings.js" as APSettings
import "javascript/Storage.js" as Storage
import "javascript/SaveandLoad.js" as SaveandLoad
import "javascript/LocationRequest.js" as LocationRequest
import "javascript/GoogleLocationAPI.js" as GoogleLocationAPI


Page{
    id: mainpage
    state: "TIMESPAGE"

    Component.onCompleted: {
        mainpage.mysunrise=LocationRequest.gettime(mylatitude,mylongitude,1)
        mainpage.mysunset=LocationRequest.gettime(mylatitude,mylongitude,0)
        GoogleLocationAPI.load(mylatitude,mylongitude)
    }

    property double mylongitude: SaveandLoad.getCoordinate("longitude")
    property double mylatitude: SaveandLoad.getCoordinate("latitude")

    property string mysunrise : "Not loaded"
    property string mysunset : "Not loaded"

    property double otherlongitude
    property double otherlatitude

    property string othersunrise : "Not loaded"
    property string othersunset : "Not loaded"


    property string locationstreet :"null"
    property string locationtown :"null"

    property bool saveposition:Storage.getSetting("SAVE_LOCATION")
    property bool savedate:Storage.getSetting("SAVE_DATE")
    property bool savetz:Storage.getSetting("SAVE_TIMEZONE")

    PositionSource {
        id: positionSource
        updateInterval: 1500
        active: !saveposition
        onPositionChanged: {
            mylongitude= positionSource.position.coordinate.longitude
            mylatitude= positionSource.position.coordinate.latitude

            mysunrise= LocationRequest.gettime(mylatitude,mylongitude,1)
            mysunset= LocationRequest.gettime(mylatitude,mylongitude,0)
        }

    }

    Item {
        id: datePerso
        Component.onCompleted: {
            if (Storage.getSetting("SAVE_DATE")=="false"){datePerso.dateperso= new Date();}
            else{datePerso.dateperso= new Date((new Date()).getFullYear(),Storage.getSetting("SAVEDATE_MONTH"),Storage.getSetting("SAVEDATE_DAY"))}

            if (Storage.getSetting("SAVE_TIMEZONE")=="false"){datePerso.timezone= (-(datePerso.dateperso.getTimezoneOffset()/60))}
            else{datePerso.timezone= Storage.getSetting("MY_TIMEZONE")}

        }
        property date dateperso
        property int timezone

    }



    TimesPage{
        id: timespage
        anchors.fill: parent
    }
    WorldPage{
        id: worldpage
        anchors.fill: parent
    }
    SettingsPage{
        id: settingspage
        anchors.fill: parent
    }
    CMWDateChooser{
        id: datechooserpage
        anchors.fill: parent
        opacity: 0
    }
    CMWTimezoneChooser{
        id: tzchooserpage
        anchors.fill: parent
        opacity: 0
    }



    states: [
        State {
            name: "TIMESPAGE"
            PropertyChanges { target: timespage; opacity:1}
            PropertyChanges { target: worldpage; opacity:0}
            PropertyChanges { target: settingspage; opacity:0}
        },
        State {
            name: "WORLDPAGE"
            PropertyChanges { target: timespage; opacity:0}
            PropertyChanges { target: worldpage; opacity:1}
            PropertyChanges { target: settingspage; opacity:0}
        },
        State {
            name: "SETTINGSPAGE"
            PropertyChanges { target: timespage; opacity:0}
            PropertyChanges { target: worldpage; opacity:0}
            PropertyChanges { target: datechooserpage; opacity:0}
            PropertyChanges { target: tzchooserpage; opacity:0}
            PropertyChanges { target: settingspage; opacity:1}
        }
        ,
        State {
            name: "DATECHOOSERPAGE"
            PropertyChanges { target: datechooserpage; opacity:1}
            PropertyChanges { target: settingspage; opacity:0}
            PropertyChanges { target: tzchooserpage; opacity:0}
        }
        ,
        State {
            name: "TZCHOOSERPAGE"
            PropertyChanges { target: tzchooserpage; opacity:1}
            PropertyChanges { target: settingspage; opacity:0}
            PropertyChanges { target: datechooserpage; opacity:0}
        }
    ]

    transitions: [
        Transition {
            to: "TIMESPAGE"
            PropertyAnimation  { target: timespage; property: "opacity"; to: 1.0; duration: 300}
            PropertyAnimation  { targets: [worldpage,settingspage]; property: "opacity"; to: 0.0; duration: 300;easing.type: Easing.InOutQuad}
        },
        Transition {
            to: "WORLDPAGE"
            PropertyAnimation  { target: worldpage; property: "opacity"; to: 1.0; duration: 300}
            PropertyAnimation  { targets: [timespage,settingspage]; property: "opacity"; to: 0.0; duration: 300;easing.type: Easing.InOutQuad}
        },
        Transition {
            to: "SETTINGSPAGE"
            PropertyAnimation  { target: settingspage; property: "opacity"; to: 1.0; duration: 300}
            PropertyAnimation  { targets: [timespage,worldpage,datechooserpage,tzchooserpage]; property: "opacity"; to: 0.0; duration: 300;easing.type: Easing.InOutQuad}
        }
        ,
        Transition {
            to: "DATECHOOSERPAGE"
            PropertyAnimation  { target: datechooserpage; property: "opacity"; to: 1.0; duration: 300}
            PropertyAnimation  { targets: [settingspage,tzchooserpage]; property: "opacity"; to: 0.0; duration: 300;easing.type: Easing.InOutQuad}
        }
        ,
        Transition {
            to: "TZCHOOSERPAGE"
            PropertyAnimation  { target: tzchooserpage; property: "opacity"; to: 1.0; duration: 300}
            PropertyAnimation  { targets: [settingspage,datechooserpage]; property: "opacity"; to: 0.0; duration: 300;easing.type: Easing.InOutQuad}
        }
    ]


}
