function load(lat,lon) {

    var xhr = new XMLHttpRequest();
    xhr.open("GET","http://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lon+"&sensor=false",true);
    xhr.onreadystatechange = function()
            {
                if ( xhr.readyState == xhr.DONE)
                {
                    if ( xhr.status == 200)
                    {
                        var jsonObject = JSON.parse(xhr.responseText);
                        return loaded(jsonObject)
                    }
                }
            }
    xhr.send();
}

function loaded(jsonObject)
{

    if(jsonObject.status!="OK")
    {
        mainpage.locationstreet="Location has failed"
        mainpage.locationtown="Please try again later"
    }

    mainpage.locationstreet=jsonObject.results[0].address_components[1].long_name
    mainpage.locationtown=jsonObject.results[0].address_components[2].long_name



}
