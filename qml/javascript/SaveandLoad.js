/**********************************************************************
 * Copyright 2012 Leopold Dauvergne
 *
 * This file is part of Clock My World.
 *
 * Clock My World is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Clock My World is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clock My World. If not, see <http://www.gnu.org/licenses/>
 *
 ***********************************************************************/


function savedate() {

    Storage.setSetting("SAVEDATE_DAY",datePerso.dateperso.getDate());
    Storage.setSetting("SAVEDATE_MONTH",datePerso.dateperso.getMonth());
}

function getdate() {

    datePerso.dateperso=new Date(datePerso.dateperso.getFullYear(),Storage.getSetting("SAVEDATE_MONTH"),Storage.getSetting("SAVEDATE_DAY"))

}

function getCoordinate(coordinate){
    var toreturn
    if (coordinate=="longitude"){
        toreturn=Storage.getSetting("MY_LONGITUDE")
    }
    else
    {
       toreturn=Storage.getSetting("MY_LATITUDE")
    }
    if (toreturn=="false")
        return 0
    else
        return toreturn
}
