// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1


import "javascript/UIConstants.js" as UIConstants
import "javascript/APSettings.js" as APSettings
import "javascript/Storage.js" as Storage
import "javascript/GoogleLocationAPI.js" as GoogleLocationAPI


Item{

    property string locationtown:""
    property string locationstreet:"null"

    Background{}

    CMWHeader{
        id:top
        title: qsTr("Clock Me")
        anchors.top:parent.top
        width:parent.width
        height: 63
    }

    Flickable {
        id: flickArea
        anchors.top: top.bottom
        height: parent.height - toolbar.height - top.height
        width: parent.width
        contentWidth: width
        contentHeight: column.height
        clip: true

        Column {
            id: column
            anchors { top: parent.top; left: parent.left; right: parent.right; margins: 16 }
            spacing: 10

            GroupSeparator {
                title: qsTr("My time")
            }
            Label {
                verticalAlignment: Text.AlignBottom
                text: qsTr("Sunrise time  : "+mainpage.mysunrise)
                height: 30
            }
            Label {
                verticalAlignment: Text.AlignBottom
                text: qsTr("Sunset time  : "+mainpage.mysunset)
                height: 30
            }

            GroupSeparator {
                title: qsTr("My location")
            }

            Item{
                id:mylocation
                height:30
                width: parent.width

                AnimationWaiting {
                    visible: mainpage.locationstreet=="null" ? true: false
                    working: mainpage.locationstreet=="null" ? true: false

                }

                Text {
                    id:gpslocationdata
                    color: "white"
                    visible: mainpage.locationstreet=="null" ? false: true
                    text: mainpage.locationtown + "\n" + mainpage.locationstreet
                    font.pointSize: UIConstants.FONT_SMALL

                }

            }



        }

    }
    InfoBanner {
        id:bannerrefresh
        text: "Refresh in progress"
        iconSource: "images/Refresh.png"
        z:2
    }
    // define the menu
    Menu {
        id: mainMenu
        content: MenuLayout {
            MenuItem {
                text: "About"
                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }

        }
    }

    ToolBar {
        id:toolbar

        anchors{
            bottom: parent.bottom
        }
        tools: ToolBarLayout {

            ToolButton {
                flat: true
                iconSource: "images/icons/quit.png"
                onClicked: {
                    Qt.quit()
                }
            }

            ToolButton {
                iconSource: "images/icons/refresh.png"
                onClicked:  {
                    mainpage.locationstreet="null"
                    mainpage.locationtown="null"
                    GoogleLocationAPI.load(mylatitude,mylongitude)
                    bannerrefresh.open()
                }
            }

            ToolButton {
                flat: true
                iconSource: "images/icons/world.png"
                onClicked:  {mainpage.state="WORLDPAGE"}//pageStack.push(Qt.resolvedUrl("WorldPage.qml"))
            }
            ToolButton {
                flat: true
                iconSource: "images/icons/settings.png"
                onClicked: {mainpage.state="SETTINGSPAGE"}//pageStack.push(Qt.resolvedUrl("SettingsPage.qml"))
            }
            ToolButton {
                iconSource: "images/icons/menu.png"
                onClicked: mainMenu.open()
            }
        }
    }





}
