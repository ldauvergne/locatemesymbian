import QtQuick 1.1
import com.nokia.symbian 1.1

import "javascript/UIConstants.js" as UIConstants
import "javascript/APSettings.js" as APSettings
import "javascript/Storage.js" as Storage

Page {
    id: disclaimerPage

    Background{}

    CMWHeader{
        id: top
        title: qsTr("About")
        anchors.top:parent.top
        width:parent.width
        height: 63
    }

    Flickable {
        id: flickArea
        anchors.top: top.bottom

        height: parent.height - toolbar.height - top.height
        width: parent.width
        contentWidth: width
        contentHeight: cmw_privacytitle.height + disclaimertext.height
        clip: true

            Text{
                id:cmw_privacytitle
                text:"Clock My World"
                color: "white"
                width: parent.width
                horizontalAlignment: Text.AlignHCenter

                anchors{
                    top: parent.top
                    topMargin: UIConstants.PADDING_XLARGE
                }

                smooth: true
                style: Text.Raised

                font{
                    family:UIConstants.FONT_FAMILY
                    pointSize: UIConstants.FONT_LARGE

                }
            }

            Text{
                id:disclaimertext
                text:"\nThis application is an open source application.\n\nBy buying it you support my work and help me to improve it.\n\nPlease contact me for any suggestion.\n\nVersion:"+APSettings.CLOCKMYWORLD_VERSION
                color: "white"
                width: parent.width
                wrapMode: Text.WordWrap

                anchors{
                    top:cmw_privacytitle.bottom
                    topMargin: UIConstants.PADDING_SMALL
                }
                horizontalAlignment: Text.AlignHCenter
                smooth: true
                style: Text.Raised
                font{
                    family:UIConstants.FONT_FAMILY
                    pointSize: UIConstants.FONT_SMALL

                }
            }



    }

    ToolBar {
        id:toolbar
        anchors{
            bottom: parent.bottom
        }

        tools: ToolBarLayout {
            id: toolBarLayout

                ToolButton {
                    id: buttonback
                    flat: true
                    anchors.left: parent.left
                    iconSource: "images/icons/back.png"
                    onClicked: pageStack.pop()
                }
                ToolButton {
                    flat: true
                    text: "Contact me"
                    anchors{
                        left: buttonback.right
                        right: parent.right
                    }

                    onClicked: Qt.openUrlExternally("mailto:"+APSettings.EMAIL+"?subject=Clock My World General Suggestion&body=Add your comment here. Please let the subject untouched.")


                }
            }


    }
}
