import QtQuick 1.1
import com.nokia.symbian 1.1


import "javascript/Storage.js" as Storage
import "javascript/APSettings.js" as APSettings


PageStackWindow {
    id: pagestackwindow
    Component.onCompleted: {

        Storage.initialize();

        if (Storage.getSetting("VERSION")!=APSettings.CLOCKMYWORLD_VERSION){
            Storage.eraseTable();
            Storage.initialize();
            Storage.setSetting("VERSION",APSettings.CLOCKMYWORLD_VERSION)
            console.log("LOG: Table erased, new version :"+APSettings.CLOCKMYWORLD_VERSION)
        }


        console.log("-"+Storage.getSetting("HIDE_DISCLAIMER"))

        if(Storage.getSetting("HIDE_DISCLAIMER")=="false"){
            return pageStack.push(Qt.resolvedUrl("DisclaimerPage.qml"))
        }
        else{
            return pageStack.push(Qt.resolvedUrl("MainPage.qml"))
        }
    }



}
