// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle{
    anchors.fill: parent
    color: "#000000"
    Image{
        id:backgroundImage
        x: 0
        z: 0
        opacity: 0.4
        height: parent.height
        rotation: 0
        source: parent.height > parent.width ? "images/wallpaper_portrait_symbian.png" : "images/wallpaper_landscape_symbian.png"

    }
}
