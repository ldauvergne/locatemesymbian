import QtQuick 1.1
import com.nokia.symbian 1.1

import "javascript/UIConstants.js" as UIConstants
import "javascript/APSettings.js" as APSettings
import "javascript/Storage.js" as Storage

Page {
    id: disclaimerPage

    Background{}

    CMWHeader{
        id: top
        title: qsTr("Disclaimer")
        anchors.top:parent.top
        width:parent.width
        height: 63
    }

    Flickable {
        id: flickArea
        anchors.top: top.bottom

        height: parent.height - toolbar.height - top.height
        width: parent.width
        contentWidth: width
        contentHeight: cmw_privacytitle.height + disclaimertext.height
        clip: true

            Text{
                id:cmw_privacytitle
                text:"Clock My World\nPrivacy Policy"
                color: "white"
                width: parent.width
                horizontalAlignment: Text.AlignHCenter

                anchors{
                    top: parent.top
                    topMargin: UIConstants.PADDING_XLARGE
                }

                smooth: true
                style: Text.Raised

                font{
                    family:UIConstants.FONT_FAMILY
                    pointSize: UIConstants.FONT_LARGE

                }
            }

            Text{
                id:disclaimertext
                text:"Clock my world is an application that use your coordinates to provide you a service.\n\n No commercial uses of your information will be made.\nThe location service is provided by Google.\n\nIf you do not wish to share your information with Google service, please quit the application."
                color: "white"
                width: parent.width
                wrapMode: Text.WordWrap

                anchors{
                    top:cmw_privacytitle.bottom
                    topMargin: UIConstants.PADDING_SMALL
                }
                horizontalAlignment: Text.AlignHCenter
                smooth: true
                style: Text.Raised
                font{
                    family:UIConstants.FONT_FAMILY
                    pointSize: UIConstants.FONT_SMALL

                }
            }



    }

    ToolBar {
        id:toolbar
        anchors{
            bottom: parent.bottom
        }

        tools: ToolBarLayout {
            id: toolBarLayout
            ButtonRow {
                anchors.centerIn: parent
                ToolButton {
                    flat: true
                    iconSource: "images/icons/quit.png"
                    onClicked: {
                        Storage.setSetting("HIDE_DISCLAIMER",false)
                        Qt.quit()
                    }
                }
                ToolButton {
                    flat: true
                    iconSource: "images/icons/valid.png"
                    onClicked:  {
                         Storage.setSetting("HIDE_DISCLAIMER",true)
                        pageStack.replace(Qt.resolvedUrl("MainPage.qml"))
                    }

                }
            }
        }

    }
}
