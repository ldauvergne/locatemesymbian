# Add more folders to ship with the application, here
folder_01.source = qml
folder_01.target =
DEPLOYMENTFOLDERS = folder_01


# Smart Installer package's UID
# This UID is from the protected range and therefore the package will
# fail to install if self-signed. By default qmake uses the unprotected
# range value if unprotected UID is defined for the application and
# 0x2002CCCF value if protected UID is given to the application
#symbian:DEPLOYMENT.installer_header = 0x2002CCCF
ICON = locatemesymbian.svg
# Allow network access on Symbian
symbian {

        TARGET.UID3 = 0x2006144a


TARGET = LocateMe
TARGET.CAPABILITY += NetworkServices Location

    my_deployment.pkg_prerules += vendorinfo
    my_deployment.pkg_prerules += "(0x200346DE), 1, 1, 0, {\"Qt Quick components\"}"


    DEPLOYMENT += my_deployment


# Add dependency to Symbian components
CONFIG += qt-components

    DEPLOYMENT.display_name += Clock World

    vendorinfo += "%{\"Leopold Dauvergne-FR\"}" ":\"Leopold Dauvergne-FR\""
}

# If your application uses the Qt Mobility libraries, uncomment the following
# lines and add the respective components to the MOBILITY variable.
# CONFIG += mobility
# MOBILITY +=

# Speed up launching on MeeGo/Harmattan when using applauncherd daemon
# CONFIG += qdeclarative-boostable

# Add dependency to Symbian components
CONFIG += qt-components
VERSION = 2.0.1
# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

OTHER_FILES += \
    qml/main.qml \
    qml/Background.qml \
    qml/javascript/UIConstants.js \
    qml/javascript/Storage.js \
    qml/javascript/SaveandLoad.js \
    qml/javascript/APSettings.js \
    qml/TimesPage.qml \
    qml/DisclaimerPage.qml
